
import boto3

def save(name, value):

    try:
        s3 = boto3.client('s3', region_name='us-east-1')
        s3.put_object(Bucket='mybucket', Key=name, Body=value)
        return True
    except:
        return False



def fetch():
    return 'OK -- fetched'

def print_here():
    print('printing here')

def print_there():
    print('printing there')
