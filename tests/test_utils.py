import boto3
from moto import mock_s3

from src.my_module import save, fetch, print_here, print_there


def test_fetch():
    value = fetch()

    assert value is not None


@mock_s3
def test_pass_my_model_save():

    conn = boto3.resource('s3', region_name='us-east-1')
    conn.create_bucket(Bucket='mybucket')

    flag = save('steve', 'is awesome')
    assert flag is True


@mock_s3
def test_fail_my_model_save():

    conn = boto3.resource('s3', region_name='us-east-1')
    conn.create_bucket(Bucket='mybucket')

    flag = save(None, None)
    assert flag is False


@mock_s3
def test_my_model_save_():


    conn = boto3.resource('s3', region_name='us-east-1')
    # We need to create the bucket since this is all in Moto's 'virtual' AWS account
    conn.create_bucket(Bucket='mybucket')

    save('steve', 'is awesome')
    body = conn.Object('mybucket', 'steve').get()['Body'].read().decode("utf-8")
    assert body == 'is awesome'


def test_print_here():
    print_here()

def test_print_there():
    print_there()


